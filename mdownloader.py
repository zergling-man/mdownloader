import argparse
import asyncio
import html
import json
import os
import re
import requests
import sys
import time

from aiohttp import ClientSession, ClientError
from tqdm import tqdm

banner    = 'The max. requests allowed are 1500/10min for the API and 600/10min for everything else. You have to wait 10 minutes or you will get your IP banned.'
headers   = { 'User-Agent': 'mDownloader/2.0' }
domain    = 'https://mangadex.org'
re_regrex = re.compile('[\\\\/:*?"<>|]')

#File containing the IDs of groups to exclude from chapter downloads. Only works for single group chapters.
blacklist_file = 'blacklist.txt'

def getLanguageName(code):

    # Read languages file
    install_path = os.path.dirname( os.path.abspath(__file__) )

    with open( os.path.join(install_path, 'languages.json'), 'r' ) as json_file:
        languages = json.load(json_file)

    return languages[code]

def createFolder(name):
    try:
        if not os.path.exists(name):
            os.makedirs(name)
            return 0
        else:
            return 1
    except OSError:
        sys.exit('Error creating folder')

async def progressDisplay(tasks):
    for f in tqdm(asyncio.as_completed(tasks), total=len(tasks)):
        try:
            await f
        except Exception as e:
            print(e)

async def imageDownloader(image, url, folder):

    retry = 0

    #try to download it 3 times
    while( retry < 3 ):
        async with ClientSession() as session:
            try:
                async with session.get( url + image ) as response:
    
                    assert response.status == 200

                    response = await response.read()

                    with open( os.path.join(folder ,image) , 'wb') as file:
                        file.write(response)

                    retry = 3

                    return { 'image': image, 'status': 'success' }
            except (ClientError, AssertionError, asyncio.TimeoutError):
                await asyncio.sleep(1)

                retry += 1

                if( retry == 3 ):
                    print( f'Could not download image {image} after 3 times.' )
                    await asyncio.sleep(1)
                    return { 'image': image, 'status': 'failed' }

def chapterDownloader( id, directory, title = '', chapter_json = [] ):

    # Check if it is downloading a title or a chapter. By default, downloading a title has a title value, so if it is empty, it is a chapter.
    type = '' != title

    # Connect to API and get chapter info
    url = f'{domain}/api/v2/chapter/{id}?saver=0'

    response = requests.get( url, headers = headers )

    # Check response codes
    code = response.status_code

    if( 200 != code ):

        error_message = 'Unknown error'

        if( code < 500 ):
            chapter_data = response.json()
            error_message = chapter_data['message']

        print( f'Error: {error_message}. Code: {code}.' )

        return { "error": error_message, "response_code": code }

    # 200 OK
    chapter_data = response.json()

    chapter_data = chapter_data['data']

    #Extenal chapters
    if( 'external' == chapter_data["status"] ):
        error = 'Chapter external to Mangadex. Unable to download.'
        print ( error )
        return { "error": error, "response_code": code }

    elif( 'delayed' == chapter_data["status"] ):
        error = 'Delayed chapter. Unable to download.'
        print ( error )
        return { "error": error, "response_code": code }

    else:
        url = f'{chapter_data["server"]}{chapter_data["hash"]}/'

        metadata = { 
            'url': url, 
            'hash': chapter_data['hash'],
            'server': chapter_data['server'],
            'volume': chapter_data['volume'],
            'chapter': chapter_data['chapter'],
            'title': chapter_data['title']
        }

        if 'serverFallback' in chapter_data:
            metadata['serverFallback'] = chapter_data['serverFallback']

        metadata['images'] = { 'success': [], 'failed': [] }

        # Get the groups
        groups = ', '.join( group['name'] for group in chapter_data['groups'] )
        groups = re_regrex.sub( '_', html.unescape( groups ) )

        # Volume naming
        volume = ''

        if ( '' != chapter_data['volume'] ):
            v = int( chapter_data['volume'] )
            volume = f'(v0{v}) ' if ( v < 10 ) else f'(v{v}) '

        # Chapter naming
        chapter = '000'

        if ( '' != chapter_data['chapter'] ):

            chapter_values = chapter_data['chapter'].split('.')

            c = int(chapter_values[0])

            if ( c < 10 ):
                chapter = f'c00{c}'
            elif ( c >= 10 and c < 100 ):
                chapter = f'c0{c}'
            elif ( c >= 100 and c < 999 ):
                chapter = f'c{c}'
            else:
                chapter = f'd{c}'

            if ( len( chapter_values ) > 1):
                for part in chapter_values[1:]:
                    chapter += '.' + part

        # Chapter name
        chapter_name = '[Oneshot]' if ( '' == chapter_data['volume'] and '' == chapter_data['chapter'] and '' == chapter_data['title'] ) else ( f'[{chapter_data["title"]}] ' if '' != chapter_data['title'] else '' )

        # Manga Title
        title = title if ( '' != title ) else chapter_data['mangaTitle']

        # Language
        language = getLanguageName( chapter_data['language'] )

        #Follows Daiz's naming scheme. (Or at least tries to)
        folder = f'{directory}/{title}/{title} [{language}] - {chapter} {volume}{chapter_name}[{groups}]'

        images = chapter_data['pages']
        resume = 0

        # Check if folder exists. If it does, check if the json for the chapter exists and download failed images. If no failed images, skip it.
        if ( createFolder(folder) ):

            # If title, check the chapter_json sent to function
            if ( type ):
                images = chapter_json['images']['failed'] if ( len( chapter_json['images']['failed'] ) > 0 ) else []
                resume = 1
            else:
                #If chapter, search for the chapter json file
                chapter_json = f'{folder}/{id}.json'

                if( os.path.exists( chapter_json ) ):
                    with open(chapter_json, 'r') as json_file:
                        chapter_json = json.load(json_file)

                    images = chapter_json['images']['failed'] if ( len( chapter_json['images']['failed'] ) > 0 ) else []
                    resume = 1

        if ( len( images ) > 0 ):

            print ( f'Downloading Volume {volume}Chapter {chapter} - Title: {chapter_name}' )

            # Images Download Tasks
            loop  = asyncio.get_event_loop()
            tasks = []

            for image in images:
                task = asyncio.ensure_future( imageDownloader( image, url, folder ) )
                tasks.append(task)

            runner = progressDisplay(tasks)
            loop.run_until_complete(runner)

            # Get the responses back
            for t in tasks:
                result = t.result()
                metadata['images'][ result['status'] ].append( result['image'] )

            if ( type ):
                return metadata
            else:
                #Update chapter metadata
                if ( resume ):
                    chapter_json['images']['success'].extend( metadata['images']['success'] )
                    chapter_json['images']['failed'] = metadata['images']['failed']

                    metadata = chapter_json

                with open( os.path.join( folder, f'{id}.json' ), 'w') as file:
                    json.dump( metadata, file, indent=4, ensure_ascii=False )

def titleDownloader(id, directory, language):

    # Connect to API and get manga info
    url = f'{domain}/api/v2/manga/{id}?include=chapters'

    response = requests.get( url, headers = headers)

    if ( response.status_code != 200 ):
        print( f'Title {id}. Request status error: {response.status_code}. Skipping...' )
        return;

    data = response.json()
    data = data['data']

    title = re_regrex.sub( '_', html.unescape( data['manga']['title'] ) )

    if 'chapters' not in data:
        print( f'Title {id} - {title} has no chapters. Skipping...' )
        return

    print( f'{"-"*69}\nDownloading Title: {title}\n{"-"*69}' )

    json_data = { 'id': id, 'title': title }
    json_data['chapters'] = {}

    # Check if the title json file exists
    title_json = os.path.join( directory, title, f'{id}.json' )

    if( os.path.exists( title_json ) ):
        with open(title_json, 'r') as json_file:
            json_data = json.load(json_file)

    # Flag for checking if at least one chapter was downloaded
    downloaded = False

    # Loop chapters
    for chapter in data['chapters']:
        # Only chapters of language selected. Default language: English.
        if ( chapter['language'] == language ):

            downloaded = True

            #List will not accept an int as index
            chapter_id = str( chapter['id'] )

            # Check if chapter id exists
            if ( chapter_id in json_data['chapters'] ):

                #Check if it has no error logged. If error, download the whole chapter
                if ( 'error' in json_data['chapters'][ chapter_id ] ):
                    json_data['chapters'][ chapter_id ] = chapterDownloader( chapter['id'], directory, title )
                else:
                    # Check if it has failed images
                    if ( len( json_data['chapters'][ chapter_id ]['images']['failed'] ) > 0 ):

                        chapter_response = chapterDownloader( chapter['id'], directory, title, json_data['chapters'][ chapter_id ] )

                        #Update success and failed images
                        json_data['chapters'][ chapter_id ]['images']['success'].extend( chapter_response['images']['success'] )
                        json_data['chapters'][ chapter_id ]['images']['failed'] = chapter_response['images']['failed']
            else:
                json_data['chapters'][ chapter_id ] = chapterDownloader( chapter['id'], directory, title )

    if ( downloaded ):
        # Create or Update json
        with open( os.path.join( directory, title, f'{id}.json' ) , 'w') as file:
            json.dump( json_data, file, indent=4, ensure_ascii=False )
    else:
        print( 'No chapters found in the selected language.' )

def fileDownloader(filename, directory, language):

    titles = []

    if( os.path.exists(filename) ):

        # Open file and read lines
        with open(filename, 'r') as item:
            titles = [ line.rstrip('\n') for line in item ]

        if( len(titles) == 0 ):
            sys.exit('Empty file!')
        else:
            print ( banner )

            for id in titles:
                titleDownloader( id, directory, language )
                print( 'Download Complete. Waiting 15 seconds...' )
                time.sleep(15)
    else:
        sys.exit('File not found!')

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()

    parser.add_argument('--language',  '-l', default='gb')         # Chapters language to download
    parser.add_argument('--directory', '-d', default='downloads2/') # Directory to download the files
    parser.add_argument('--type',      '-t', default='title')      # Type of download. Can be title or chapter. This is used when not downloading from list. Planned downloading from Groups and Users.
    parser.add_argument('id')                                      # Title or Chapter ID

    args = parser.parse_args()

    # Downloading a Title or Using the file download
    if ( args.type == 'title' ):

        # Not a number, so it must be a file.
        if ( not args.id.isdigit() ):
            fileDownloader( args.id, args.directory, args.language )
        else:
            print ( banner )
            titleDownloader( args.id, args.directory, args.language )
    elif ( args.type == 'chapter' ):

        if ( not args.id.isdigit() ):
            sys.exit('ID must be number for chapter downloads.')
        else:
            print ( banner )
            chapterDownloader( args.id, args.directory )
    else:
        sys.exit('Invalid options.')

